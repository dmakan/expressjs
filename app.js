var express = require('express');
var conn = require("./config/db")

var app = express();
app.set("view engine", "ejs");

// fichier static
app.use(express.static("public"))

// json
app.use(express.json())

// PORT 
const PORT = 8000;

// la page d'accueille
app.get('/', function(req, res) {
  context = {};
  res.render('page/index', context);
});


// recuperation de donnee dans la db
app.get('/api', function(req, res) {
  // res.setHeader('Access-Control-Allow-Origin', "*")
  // res.setHeader('Content-Type', 'Application/json')
  conn.query("SELECT * FROM parking", (error, result) => {
    if (error) throw error;
    res.json(result)
  })
  // conn.end();
  // res.end()
});

// recuperation de l'id passé en parametre
app.get('/api/:id', function(req, res) {
  let id = parseInt(req.params.id)
  conn.query(`SELECT * FROM parking WHERE id=${id}`, (error, result) => {
    if (error) throw error;
    res.json(result)
  })
  // res.end()
});

// edition de données (update)
app.put('/update/:id', function(req, res) {
  let id = parseInt(req.params.id)
  let name = req.body.name 
  let city = req.body.city 
  let type = req.body.type
  let Id = req.body.id 

  conn.query(`UPDATE parking SET name='${name}', city='${city}', type='${type}' WHERE id=${id}` , (error, result) => {
    if (error) throw error;
    console.log("les donnés ont été modifié avec succès");
  })

});


//  suppression des données (id passé en parametre)
app.delete('/delete/:id', function(req, res) {
  let id = parseInt(req.params.id)
  
  conn.query("DELETE FROM parking WHERE id="+id, (error, result) => {
    if (error) throw error;
    console.log("les données ont été supprimé avec succès");
  })
});

// Message dans la console pour indiquer le port
app.listen(PORT, () => {
    console.log(`En ecoute sur le port : ${PORT}`)
  })