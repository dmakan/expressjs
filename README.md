# API NodeJS

construction d'API avec le serveur NodeJS. c'est programme manipule des données de db, effectuer des actions C.R.U.D (Create, Read, Update, Delete)
C'est programme est conçu avec le seveur NodeJS et Framework ExpressJS.

Pour le bon fonctionnement de ce programme veuillez installer tous les dependances necessaire.

# Dependances
<ul>
<li>express</li>
<li>mysql</li>
<li>mongodb</li>
<li>sqlite3</li>
<li>ejs</li> 
</ul>

# Installation de dependances
cd myapp
```
npm init
npm install express --save
npm install mysql --save
npm install ejs --save
npm install nodemon --save
npm install mongodb --save
npm install sqlite3 --save
```